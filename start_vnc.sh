#!/bin/bash

echo "___starting vnc stuff"
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update
sudo apt-get install -y python2-minimal 

sudo systemctl stop gdm
sudo systemctl disable gdm

sudo patch /usr/bin/geni-get /local/repository/geni-get.patch

TMPDIR=$(mktemp -d -t vnc-XXXXXXXX)

if [ ! -e $TMPDIR/manifest.xml ]; then
    geni-get manifest > $TMPDIR/manifest.xml
    cat $TMPDIR/manifest.xml | grep -q emulab:password
    if [ $? -ne 0 ]; then
        echo "ERROR: geni-get manifest failed or no password block!"
        exit 1
    fi
fi

if [ ! -e $TMPDIR/geni.key ]; then
    geni-get key > $TMPDIR/geni.key
    cat $TMPDIR/geni.key | grep -q END\ .\*\PRIVATE\ KEY
    if [ $? -ne 0 ]; then
        echo "ERROR: geni-get key failed!"
        exit 1
    fi
fi

if [ ! -e $TMPDIR/encrypted_vncpasswd ]; then
    cat $TMPDIR/manifest.xml | perl -e '@lines = <STDIN>; $all = join("",@lines); if ($all =~ /^.+<[^:]+:password[^>]*>([^<]+)<\/[^:]+:password>.+/igs) { print $1; }' > $TMPDIR/encrypted_vncpasswd
fi

if [ ! -e $TMPDIR/decrypted_vncpasswd -a -s $TMPDIR/encrypted_vncpasswd ]; then
    openssl smime -decrypt -inform PEM -inkey $TMPDIR/geni.key -in $TMPDIR/encrypted_vncpasswd -out $TMPDIR/decrypted_vncpasswd
    if [ $? -ne 0 ]; then
        echo "ERROR: openssl decrypt failed!"
        exit 1
    fi
fi


sudo iptables -A INPUT -m tcp -p tcp --dport 5901 -j ACCEPT

screen -S Xvfb -d -m /bin/bash -c "Xvfb -screen 0 1920x1080x24 :99; exec /bin/bash"
sleep 10 # Maybe machine is busy first time through or something??
screen -S x11vnc -d -m /bin/bash -c "x11vnc -display :99 -forever -nevershared -passwd `cat $TMPDIR/decrypted_vncpasswd` -rfbport 5901; exec /bin/bash"
DISPLAY=:99 screen -S xfce4 -d -m /bin/bash -c "xfce4-session; exec /bin/bash"
echo "___finished VNC starting "
