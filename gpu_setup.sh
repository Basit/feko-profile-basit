#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

export DISTRO=ubuntu1804
export ARCHITECTURE=x86_64
echo "___updating lists for nvidia stuff"
sudo apt-get clean
sudo rm -rf /var/lib/apt/lists/*
# sudo apt-get update
# sudo add-apt-repository -y ppa:graphics-drivers
echo "___Distro shows as "$DISTRO
echo "___Arch   shows as "$ARCHITECTURE

# adding these in to purge the system of current nvidia and cuda drivers
sudo rm /etc/apt/sources.list.d/cuda.list
sudo rm /etc/apt/sources.list.d/cuda_learn.list
sudo apt-key del 7fa2af80
sudo apt-get purge nvidia-*
sudo apt-get purge cuda-*

sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/3bf863cc.pub
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/machine-learning/repos/${DISTRO}/${ARCHITECTURE}/7fa2af80.pub
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/\ / > /etc/apt/sources.list.d/cuda.list"
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/machine-learning/repos/${DISTRO}/${ARCHITECTURE}\ / > /etc/apt/sources.list.d/cuda_learn.list"
echo "___finished updating lists"
sudo apt-get update
sudo apt-get upgrade -y apt
# Needs to be <=470 to work with Tesla K80
sudo apt-get install -y --allow-change-held-packages nvidia-driver-470 libnvidia-gl-470 libnvidia-common-470 nvidia-dkms-470 nvidia-kernel-source-470 nvidia-compute-utils-470 nvidia-utils-470 xserver-xorg-video-nvidia-470 libnvidia-cfg1-470 libnvidia-compute-470 libnvidia-decode-470 libnvidia-encode-470 libnvidia-ifr1-470 libnvidia-fbc1-470 libnvidia-gl-470 nvidia-kernel-common-470
# This pacakge signals cuda-runtime what version to use (and prevents
# uninstalling 470 and switching to 510)
sudo apt-get install -y --allow-change-held-packages cuda=11.4.4-1 cuda-drivers=470.129.06-1
echo "___doing for loop for packages"
for pkg in nvidia-driver-470 libnvidia-gl-470 libnvidia-common-470 nvidia-dkms-470 nvidia-kernel-source-470 nvidia-compute-utils-470 nvidia-utils-470 xserver-xorg-video-nvidia-470 libnvidia-cfg1-470 libnvidia-compute-470 libnvidia-decode-470 libnvidia-encode-470 libnvidia-ifr1-470 libnvidia-fbc1-470 libnvidia-gl-470 nvidia-kernel-common-470 cuda-drivers-470
do
    echo "${pkg} hold" | sudo dpkg --set-selections
done
echo "___installing cuda 11-4"
sudo apt-get install -y --allow-change-held-packages  cuda=11.4.4-1 cuda-drivers=470.129.06-1
sudo modprobe nvidia
sleep 5
nvidia-smi

echo "___finished gpu_setup sh script"
# change above to cuda=11.4.4-1 ... as seen here https://forums.developer.nvidia.com/t/ubuntu-install-specific-old-cuda-drivers-combo/214601/5
