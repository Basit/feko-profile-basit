#!/bin/bash
echo "___ Starting the install of Feko:"
sudo useradd fekouser

dir /opt/ | grep feko > /dev/null
if [ $? -eq 1 ]; then
    echo "___Pulling feko files.."
    cd /opt/feko && sudo wget --output-document feko_2022.zip "https://drive.google.com/uc?export=download&id=1lfzdetmI50kesXuBqzsz-FazjblWHAT8&confirm=yes"
    echo "___Unzipping feko to /opt/feko."
    sudo unzip feko_2022.zip
else
    echo "___Install already completed before. will not copy again."
fi

echo "___Setting up the profile info for feko paths"
sudo touch appending_to_profile.sh
sudo cat /etc/.profile | sudo grep altair
if [ $? -eq 1 ]; then
    sudo echo "

    # sourcing altair init
    source /opt/feko/2022/altair/feko/bin/initfeko

    ALTAIR_PATH=\"/opt/feko/2022/altair/feko/bin\"
    if [ \$( (echo \$PATH) | grep 2022/altair) ]; then
	    echo \"Altair already present already in path.\"
    else
	    export PATH=\$PATH:\$ALTAIR_PATH
    fi

    if [ \$( (echo \$ALTAIR_WINPROP) | grep winprop) ]; then
	    echo \"winprop dir already set up\"
    else
	    export ALTAIR_WINPROP=\"/opt/feko/2022/altair/feko/api/winprop/\"
    fi
    export ALTAIR_LICENSE_PATH='6200@winlic-d.eng.utah.edu'

    if ! echo \$LD_LIBRARY_PATH | grep -q '/opt/feko/2022/altair/feko/api/winprop/bin'; then
      export LD_LIBRARY_PATH=\"/opt/feko/2022/altair/feko/api/winprop/bin\":\$LD_LIBRARY_PATH;
      echo 'Path added to LD_LIBRARY_PATH.';
    else
      echo 'Path already in LD_LIBRARY_PATH. No action taken.';
    fi

    " | sudo tee -a  appending_to_profile.sh > /dev/null

    sudo chmod u+x appending_to_profile.sh
    echo "___executing append to profile"
    sudo ./appending_to_profile.sh

    echo "___Pushing contents of append to profile to /etc/.profile"
    sudo sh -c 'cat appending_to_profile.sh >> /etc/.profile'
fi

echo "___testing Winprop CLI at opt/feko/2022/altair/feko/bin"
source /etc/.profile

/opt/feko/2022/altair/feko/bin/WinPropCLI --help

if [ $? -eq 0 ]; then
    echo -e "___WinPropCLI setup successful!!"
else
    echo -e "___WinPropCLI setup failure :/"
fi


