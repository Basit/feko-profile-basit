import geni.portal as portal
import geni.rspec.pg as rspec

# show as unused, but leaving in just in case
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


def make_profile(disk_image, dataset=None, setup=False):
    portal.context.defineParameter("node_type",
                                   "Type of node to get (e.g. d740, d840, "
                                   "etc)",
                                   portal.ParameterType.STRING, "d740")
    portal.context.defineParameter("request_gpu",
                                   "Whether to request a GPU",
                                   portal.ParameterType.BOOLEAN, False)
    portal.context.defineParameter("force_gpu",
                                   "Whether to request a GPU and fail if "
                                   "one is not available",
                                   portal.ParameterType.BOOLEAN, False)

    if dataset is None:
        portal.context.defineParameter("dataset",
                                       "URN of the dataset to use",
                                       portal.ParameterType.STRING, "")

    params = portal.context.bindParameters()
    request = portal.context.makeRequestRSpec()

    if dataset is None:
        dataset = params.dataset

    node = request.RawPC("node")
    node.hardware_type = params.node_type
    node.disk_image = disk_image
    bs = node.Blockstore("bs", "/fekoStuff")
    bs.size = "400GB"

    desire_gpu = None
    if params.request_gpu:
        desire_gpu = 0.99

    if params.force_gpu:
        desire_gpu = 1.0

    if desire_gpu is not None:
        node.Desire("GPU", desire_gpu)

    if setup:
        node.addService(
            rspec.Execute(shell="bash",
                          command="/local/repository/feko_setup.sh"))
        node.addService(
            rspec.Execute(shell="bash",
                          command="/local/repository/globalProtect_setup.sh"))
        # node.addService(
        #     rspec.Execute(shell="bash",
        #                   command="/local/repository/gpu_setup.sh"))
        # node.addService(
        #     rspec.Execute(shell="bash",
        #                   command="/local/repository/setup_vnc.sh"))
        # node.addService(
        #     rspec.Execute(shell="bash",
        #                   command="/local/repository/ubuntu20-deps.sh"))
        # node.addService(
        #     rspec.Execute(shell="bash",
        #                   command="/local/repository/ubuntu20-more-deps.sh"))
        # node.addService(
        #     rspec.Execute(shell="bash",
        #                   command="/local/repository/license_setup.sh"))

    node.addService(
        rspec.Execute(shell="bash",
                      command="/local/repository/start_vnc.sh"))

   

    # iSCSI blockstore
    iface = node.addInterface()
    fsnode = request.RemoteBlockstore("fsnode", "/opt/feko")
    fsnode.dataset = dataset
    fsnode.readonly = True
    

    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)

    fslink.best_effort = True
    fslink.vlan_tagging = True

    # noVNC
    node.startVNC(True)

    portal.context.printRequestRSpec()
