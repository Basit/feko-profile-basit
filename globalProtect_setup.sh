#!/bin/bash
echo "___ Starting the install of GlobalProtect:"
sudo useradd gpuser
sudo add-apt-repository ppa:yuezk/globalprotect-openconnect
sudo apt-get update 
sudo apt install -y globalprotect-openconnect
echo "___Completed."

dir /opt/ | grep feko_files > /dev/null
if [ $? -eq 1 ]; then
    echo "___Pulling feko_files repo"
    cd /opt/feko/ && sudo git clone https://gitlab.flux.utah.edu/mgomez-01/feko_files.git
    echo "___Completed pull"
else
    echo "___feko_files already present. no need to pull."
fi
